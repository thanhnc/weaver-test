class CreateBuyRecords < ActiveRecord::Migration
  def change
    create_table :buy_records do |t|
      t.integer :user_id
      t.integer :good_id
      t.integer :to_user
      
      t.timestamps
    end
  end
end
