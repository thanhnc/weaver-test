class User < ActiveRecord::Base
  attr_accessible :email, :password_digest, :username, :budget

  has_many :buy_records
  has_many :goods, through: :buy_records

  def buy good_id, to_user
    good = Good.find_by_id(good_id)
    if !good.nil? && good.price < self.budget
      buy_records.create(good_id: good_id, to_user: to_user)
      self.update_attributes(budget: self.budget - good.price)
      return true
    end
    return false
  end
end
