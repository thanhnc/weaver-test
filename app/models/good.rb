class Good < ActiveRecord::Base
  attr_accessible :description, :name, :price

  has_many :buy_records
  has_many :user, through: :buy_records
end
