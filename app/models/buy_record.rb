class BuyRecord < ActiveRecord::Base
  attr_accessible :good_id, :user_id, :to_user

  belongs_to :user
  belongs_to :good
end
