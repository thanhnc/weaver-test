class BuyRecordsController < ApplicationController
  def create
    # binding.pry
    user = current_user
    success_transaction = user.buy(params[:buy_record][:good_id], params[:buy_record][:to_user])
    if success_transaction
      redirect_to good_path(params[:buy_record][:good_id]), flash: { success: "Done" }
    else 
      redirect_to good_path(params[:buy_record][:good_id]), flash: { error: "Not enough money" }
    end
  end

  def index
    @buy_records = BuyRecord.where(user_id: current_user.id)
  end
end
